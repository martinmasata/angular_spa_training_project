import { Injectable } from '@angular/core';
import {HttpClient } from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

constructor(
  private router: Router, private hhtpClient: HttpClient
) { }

authenticate(username, password) {
  return this.hhtpClient.post<any>('https://AAAlogintest.free.beeceptor.com/login' , {username, password}).pipe(
    map(
      userData => {
        sessionStorage.setItem('username' , username);
        sessionStorage.setItem('roles' , userData.roles);
        sessionStorage.setItem('token' , 'Bearer ' + userData.token);
        return userData;
      }
    )
  );
}

isUserLoggedIn() {
  return !(sessionStorage.getItem('username') === null);
}

logout() {
  sessionStorage.removeItem('username');
  sessionStorage.removeItem('roles');
  this.router.navigate(['']);

}

}
