import { Injectable } from '@angular/core';

@Injectable()
export class NewsService {
  sections = [
    {value: 'none' , viewValue: 'Neuvedeno'},
    {value: 'csgo' , viewValue: 'Counter Strike: Global offensive'},
    {value: 'lol' , viewValue: 'League of legends'},
    {value: 'hs' , viewValue: 'Hearthstone'},
    {value: 'dota' , viewValue: 'Dota 2'}
  ];
  orders = [
    {value: 'dateAsc' , viewValue : 'Datum: od nejnovějšího'},
    {value: 'dateDesc' , viewValue: 'Datum: od nejstaršího'},
    {value: 'titleAsc' , viewValue: 'Název: abecedně A-Z'},
    {value: 'titleDesc' , viewValue: 'Název: abecedně Z-A'}
  ];
  newsArr = [
    {id: 1 , section: 'lol' , title : 'Nazev clanku REST' , date : '10.12.2021'},
    {id: 2 , section: 'csgo' , title : 'Nazev clanku REST2' , date : '10.12.2021'},
    {id: 3 , section: 'hs' , title : 'Nazev clanku REST3' , date : '10.12.2021'},
    {id: 11 , section: 'lol' , title : 'Nazev clanku REST' , date : '10.12.2021'},
    {id: 22 , section: 'csgo' , title : 'Nazev clanku REST2' , date : '10.12.2021'},
    {id: 23 , section: 'hs' , title : 'Nazev clanku REST3' , date : '10.12.2021'},
    {id: 3 , section: 'hs' , title : 'Nazev clanku REST3' , date : '10.12.2021'},
    {id: 11 , section: 'lol' , title : 'Nazev clanku REST' , date : '10.12.2021'},
    {id: 22 , section: 'csgo' , title : 'Nazev clanku REST2' , date : '10.12.2021'},
    {id: 3 , section: 'hs' , title : 'Nazev clanku REST3' , date : '10.12.2021'},
    {id: 3 , section: 'hs' , title : 'Nazev clanku REST3' , date : '10.12.2021'}
  ];
  newsLength = 11;

constructor() { }


getSections(){
  return this.sections;
}

getOrders(){
  return this.orders;
}

getNewsPreview(){
  return this.newsArr;
}

getLength(){
  return this.newsLength;
}

getNextNewsPreview(){
  return [{id: 99999 , section: 'dota' , title: 'Dota', date: '999.999.9999'}];
}

}
