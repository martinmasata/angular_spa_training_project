import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  card1 = {
    title : 'Title aktuality',
    subtitle : 'Pod title',
    content : 'toto je obsah teto aktuality. Vice zde blabla'

  };

  constructor() { }

  ngOnInit() {
  }

}
