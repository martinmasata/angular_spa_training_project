import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username = '';
  password = '';
  invalidLogin = false;
  spinnerIsHidden = true;
  showErrorMessageDialog = false;

  constructor(private router: Router,
              private loginService: AuthenticationService) { }

  ngOnInit() {
  }

  errorMessageDialog() {
    this.showErrorMessageDialog = true;
    setTimeout (() => {
      this.showErrorMessageDialog = false;
   }, 2000);
  }

  checkLogin() {
    this.spinnerIsHidden = false;
    (this.loginService.authenticate(this.username , this.password).subscribe(
      data => {
        this.invalidLogin = false;
        this.spinnerIsHidden = true;
        this.router.navigate(['']);
      },
      error => {
        this.invalidLogin = true;
        this.spinnerIsHidden = true;
        this.errorMessageDialog();
      }
    ));
  }

}
