import { Component, OnInit } from '@angular/core';
import { NewsService} from '../../services/news.service';
import {PageEvent} from '@angular/material/paginator';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css'],
  providers: [NewsService]
})
export class NewsComponent implements OnInit {
  selectedOrder = 'dateAsc';
  selectedSection = 'none';
  currentPage = 0;
  pageSizeOptions: number[] = [5, 10, 25, 100];
  pageSize = 10;


  sections;
  orders;
  newsArr;
  length;
  pageEvent: PageEvent;

  constructor(
    private newsService: NewsService
    ) { }

  ngOnInit() {
    this.sections = this.newsService.getSections();
    this.orders = this.newsService.getOrders();
    this.newsArr = this.newsService.getNewsPreview();
    this.length = this.newsService.getLength();
  }

  reloadData(e: any){
    if(e.pageIndex > this.currentPage){
      this.newsArr = this.newsService.getNextNewsPreview();
    } else {
      this.newsArr = this.newsService.getNewsPreview();
    }
    this.currentPage = e.pageIndex;
  }

}
