import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

export interface Month {
  dateName: string;
  dayName: string;
  cols: number;
  rows: number;
  text: string;
}

@Component({
  selector: 'app-calendary',
  templateUrl: './calendary.component.html',
  styleUrls: ['./calendary.component.css']
})
export class CalendaryComponent implements OnInit {
  numberOfCols: number;
  daysInCurrentMonth: Month[] = [
    {dateName: '1. Pro', cols: 1, rows: 1, text: 'udalost 1' , dayName: 'Po'},
    {dateName: '2. Pro', cols: 1, rows: 1, text: 'udalost 1' , dayName: 'Út'},
    {dateName: '3. Pro', cols: 1, rows: 1, text: 'udalost 1' , dayName: 'St'},
    {dateName: '4. Pro', cols: 1, rows: 1, text: 'udalost 1' , dayName: 'Čt'},
    {dateName: '5. Pro', cols: 1, rows: 1, text: 'udalost 1' , dayName: 'Pá'},
    {dateName: '6. Pro', cols: 1, rows: 1, text: 'udalost 1', dayName: 'So'},
    {dateName: '7. Pro', cols: 1, rows: 1, text: 'udalost 1', dayName: 'Ne'},
    {dateName: '8. Pro', cols: 1, rows: 1, text: 'udalost 1', dayName: 'Po'},
    {dateName: '9. Pro', cols: 1, rows: 1, text: 'udalost 1', dayName: 'Út'},
    {dateName: '10. Pro', cols: 1, rows: 1, text: 'udalost 1', dayName: 'St'},
    {dateName: '11. Pro', cols: 1, rows: 1, text: 'udalost 1', dayName: 'Čt'},
    {dateName: '12. Pro', cols: 1, rows: 1, text: 'udalost 1', dayName: 'Pá'},
    {dateName: '13. Pro', cols: 1, rows: 1, text: 'udalost 1', dayName: 'So'},
    {dateName: '14. Pro', cols: 1, rows: 1, text: 'udalost 1', dayName: 'Ne'},
    {dateName: '15. Pro', cols: 1, rows: 1, text: 'udalost 1', dayName: 'Po'},
    {dateName: '16. Pro', cols: 1, rows: 1, text: 'udalost 1', dayName: 'Út'},
    {dateName: '17. Pro', cols: 1, rows: 1, text: 'udalost 1', dayName: 'St'},
    {dateName: '18. Pro', cols: 1, rows: 1, text: 'udalost 1', dayName: 'Čt'},
    {dateName: '19. Pro', cols: 1, rows: 1, text: 'udalost 1', dayName: 'Pá'},
    {dateName: '20. Pro', cols: 1, rows: 1, text: 'udalost 1', dayName: 'So'},
    {dateName: '21. Pro', cols: 1, rows: 1, text: 'udalost 1', dayName: 'Ne'},
    {dateName: '22. Pro', cols: 1, rows: 1, text: 'udalost 1', dayName: 'Po'},
    {dateName: '23. Pro', cols: 1, rows: 1, text: 'udalost 1', dayName: 'Út'},
    {dateName: '24. Pro', cols: 1, rows: 1, text: 'udalost 1', dayName: 'St'},
    {dateName: '25. Pro', cols: 1, rows: 1, text: 'udalost 1', dayName: 'Čt'},
    {dateName: '26. Pro', cols: 1, rows: 1, text: 'udalost 1', dayName: 'Pá'},
    {dateName: '27. Pro', cols: 1, rows: 1, text: 'udalost 1', dayName: 'So'},
    {dateName: '28. Pro', cols: 1, rows: 1, text: 'udalost 1', dayName: 'Ne'},
    {dateName: '29. Pro', cols: 1, rows: 1, text: 'udalost 1', dayName: 'Po'},
    {dateName: '30. Pro', cols: 1, rows: 1, text: 'udalost 1', dayName: 'Út'},
    {dateName: '31. Pro', cols: 1, rows: 1, text: 'udalost 1', dayName: 'St'},
    {dateName: '1. Led', cols: 1, rows: 1, text: 'udalost 1', dayName: 'Čt'},
    {dateName: '2. Led', cols: 1, rows: 1, text: 'udalost 1', dayName: 'Pá'},
    {dateName: '3. Led', cols: 1, rows: 1, text: 'udalost 1', dayName: 'So'},
    {dateName: '4. Led', cols: 1, rows: 1, text: 'udalost 1', dayName: 'Ne'},
  ];

  constructor(private router: Router) { }

  changeNumOfCols() {
    if (window.innerWidth > 800) {
      this.numberOfCols = 7;
    } else if (window.innerWidth <= 800 && window.innerWidth > 500) {
      this.numberOfCols = 4;
    } else if (window.innerWidth <= 500) {
      this.numberOfCols = 3;
    }
  }

  ngOnInit() {
    this.changeNumOfCols();
  }

  onResize(event) {
    this.changeNumOfCols();
  }

  redirectToDetailDay(d: Month) {
    this.router.navigate(['calendary/' + d.dateName]);
  }

}
