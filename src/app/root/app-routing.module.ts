import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NewsComponent } from '../components/news/news.component';
import { CalendaryComponent } from '../components/calendary/calendary.component';
import { HomeComponent } from '../components/home/home.component';
import { TournamentsComponent } from '../components/tournaments/tournaments.component';
import { LoginComponent } from '../components/login/login.component';
import { RegisterComponent } from '../components/register/register.component';
import { NewsDetailComponent } from '../components/news/news-detail/news-detail.component';
import {CalendaryDetailComponent} from '../components/calendary/calendaryDetail/calendaryDetail.component';


const routes: Routes = [
  {path: '' , component : HomeComponent},
  {path: 'news' , component : NewsComponent} ,
    { path: 'news/:id', component: NewsDetailComponent },
  {path : 'calendary' , component : CalendaryComponent},
    {path: 'calendary/:date' , component: CalendaryDetailComponent},
  {path : 'tournaments' , component : TournamentsComponent},
  {path : 'login' , component : LoginComponent},
  {path : 'register' , component : RegisterComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
