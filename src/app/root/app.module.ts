import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatToolbarModule , MatButtonModule , MatNativeDateModule } from '@angular/material';
import {MatSidenavModule} from '@angular/material/sidenav';
import { FlexLayoutModule } from '@angular/flex-layout';
import {MatIconModule} from '@angular/material/icon';
import {MatMenuModule} from '@angular/material/menu';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatSelectModule} from '@angular/material/select';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatDialogModule} from '@angular/material/dialog';
import { FormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MainMenuComponent } from '../components/main-menu/main-menu.component';
import { NewsComponent } from '../components/news/news.component';
import { CalendaryComponent } from '../components/calendary/calendary.component';
import { HomeComponent } from '../components/home/home.component';
import { TournamentsComponent } from '../components/tournaments/tournaments.component';
import { LoginComponent } from '../components/login/login.component';
import { RegisterComponent } from '../components/register/register.component';
import { NewsDetailComponent } from '../components/news/news-detail/news-detail.component';
import {CalendaryDetailComponent} from '../components/calendary/calendaryDetail/calendaryDetail.component';

@NgModule({
  declarations: [
    AppComponent ,
    MainMenuComponent ,
    NewsComponent ,
    CalendaryComponent,
    HomeComponent,
    TournamentsComponent,
    LoginComponent,
    RegisterComponent,
    NewsDetailComponent,
    CalendaryDetailComponent
  ],
  imports: [
    MatPaginatorModule,
    MatSelectModule,
    MatNativeDateModule,
    MatDatepickerModule,
    MatInputModule,
    MatCardModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatMenuModule,
    MatDialogModule,
    MatFormFieldModule,
    MatProgressSpinnerModule,
    MatGridListModule,
    MatTooltipModule,

    FlexLayoutModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
